import json
import logging
import os
import sys
import re
import time
import subprocess

from selenium import webdriver
from selenium.webdriver.common.by import By

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

domain = "kobo.com"
base_url = f"https://{domain}/tw/zh"
library_url = f"{base_url}/library"
cookie_file = f"{domain}_cookie.json"
acsms_path = os.path.dirname(os.path.realpath(__file__)) + '/acsms'
output_path = os.path.dirname(os.path.realpath(__file__)) + '/ebooks'

if not os.path.exists(acsms_path):
    os.makedirs(acsms_path, exist_ok=True)

if not os.path.exists(output_path):
    os.makedirs(output_path, exist_ok=True)

if os.path.exists('kobo_raw_cookie'):
    cookie_infos = []
    with open('kobo_raw_cookie') as raw_cookie:
        cookie_string = raw_cookie.readline()
        for cookie in cookie_string.split(";"):
            r = re.match(r"([a-zA-Z_-]+)=(.*)", cookie.strip())
            if r and len(r.groups()) == 2:
                cookie_infos.append({
                    "name": r[1],
                    "value": r[2]
                })
        raw_cookie.close()
        os.remove("kobo_raw_cookie")
    if cookie_infos:
        with open(cookie_file, 'w') as file:
            file.write(json.dumps(cookie_infos))
            file.close()

def browser_initial():
    logger.info('Init WebDriver')
    options = webdriver.ChromeOptions()
    prefs = {
        "profile.default_content_settings.popups": 0,
        "download.default_directory": acsms_path,
        "download.prompt_for_download": False,
        "download.directory_upgrade": True
    }
    options.add_argument('--headless=new')
    options.add_experimental_option("prefs", prefs)
    browser = webdriver.Chrome(options=options)
    browser.get(base_url)
    load_cookie(browser)
    return browser

def get_cookie_dict(key, value):
    return {
        "domain": f".{domain}",
        "name": key,
        "value": value,
        "expires": '',
        "path": "/",
        "httpOnly": False,
        "HostOnly": False,
        "Secure": False
    }

def load_cookie(browser):
    logger.info('Loadding Cookie....')
    cookies_list = []
    with open(cookie_file) as cookie_f:
        cookies_list = json.load(cookie_f)
        cookie_f.close()
    for cookie in cookies_list:
        browser.add_cookie(get_cookie_dict(cookie.get('name'), cookie.get('value')))
    browser.refresh()

def get_next_page(browser):
    next_page_element = browser.find_element(By.CLASS_NAME, 'next')
    return next_page_element.get_attribute('href')

def get_book_infos(browser) -> list:
    infos = []
    for file in browser.find_elements(By.CLASS_NAME, 'export-file'):
        infos.append(json.loads(file.get_attribute('data-kobo-gizmo-config')))
    return infos

def download_acsm(browser, url, name) -> None:
    browser.get(url)
    raw_path = f"{download_path}/URLLink.acsm"
    new_path = f"{download_path}/{name}.acsm"
    if os.path.exists(new_path):
        return
    logger.info(f"Download {name}.acsm")
    while not os.path.exists(raw_path):
        logger.info('Waitting download finished')
        time.sleep(2)
    os.rename(raw_path, new_path)


if __name__ == "__main__":
    book_infos = []
    browser = browser_initial()
    logger.info('Loadding library ....')
    browser.get(library_url)
    book_infos = get_book_infos(browser)
    while next_page_url := get_next_page(browser):
        logger.info(f"Goto {next_page_url}")
        browser.get(next_page_url)
        book_infos += get_book_infos(browser)

    print(book_infos)
    with open('kobo_books_info.json', 'w') as info_file:
        info_file.write(json.dumps(book_infos))
        info_file.close()

    for book in book_infos:
        download_acsm(browser, book["downloadUrls"][0]["url"], book["title"])

    logger.info("Convert file....")
    subprocess.run("cd "+acsms_path+' && ls *.acsm | xarge -I {} knock {}', shell=True)
    logger.info("Convert finished")

    browser.quit()
